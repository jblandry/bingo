/** @type {import('tailwindcss').Config} */
module.exports = {
	content: ["./src/**/*.{js,jsx,ts,tsx}"],
	theme: {
		fontFamily: {
			sans: ["Inconsolata", "Helvetica", "Arial", "sans-serif"],
		},
		extend: {
			gridTemplateRows: {
				board: "repeat(15, auto)",
				latest: "repeat(10, auto)",
			},
		},
	},
	plugins: [],
};
