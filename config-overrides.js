const path = require("node:path");

module.exports = {
	paths: function (paths, env) {
		paths.appPublic = path.resolve(`${__dirname}/public_local`);
		paths.appHtml = path.resolve(`${__dirname}/public_local/index.html`);
		paths.appBuild = path.resolve(`${__dirname}/public`);
		return paths;
	},
};
