import { useAppContext } from "../contexts/AppContext";

const styles = {
	header: "grid grid-cols-5 border border-black py-4 border-b-0 mt-8 mx-8 font-bold text-center",
	board: "grid grid-rows-board grid-flow-col border border-black mx-8 text-center",
	tile: "bg-gray-100 text-gray-300",
	tileActive: "bg-gray-700 font-bold text-gray-100",
};

const Board = function () {
	const { getMarbles, getDrawn } = useAppContext();

	return (
		<div>
			<ul className={styles.header}>
				{[..."BINGO"].map((letter) => (
					<li key={letter}>{letter}</li>
				))}
			</ul>

			<ul className={styles.board}>
				{getMarbles().map((value) => (
					<li key={value} className={getDrawn().includes(value) === true ? styles.tileActive : styles.tile}>
						{value}
					</li>
				))}
			</ul>
		</div>
	);
};

export default Board;
