const styles = {
	root: "p-4 mb-4 bg-gray-300 text-xl font-bold text-center",
};

const Header = function () {
	return <header className={styles.root}>Bingo</header>;
};

export default Header;
