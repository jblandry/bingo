import { useAppContext } from "../contexts/AppContext";

const styles = {
	root: "mx-8 text-center",
	draw: "bg-green-300 px-4 py-2 border border-green-500 mx-4 text-green-900 disabled:bg-gray-300 disabled:border-gray-300 disabled:text-gray-400",
	reset:
		"bg-red-300 px-4 py-2 border border-red-400 mx-4 text-red-900 disabled:bg-gray-300 disabled:border-gray-300 disabled:text-gray-400",
};

const Actions = function () {
	const { getRemaining, getDrawn, draw, reset } = useAppContext();

	const handleDraw = () => {
		draw();
	};

	const handleReset = () => {
		// eslint-disable-next-line no-alert
		if (confirm("Êtes-vous sûr?")) {
			reset();
		}
	};

	return (
		<div className={styles.root}>
			<button onClick={handleDraw} disabled={getRemaining().length <= 0} className={styles.draw} type="button">
				Piger
			</button>
			<button onClick={handleReset} disabled={getDrawn().length <= 0} className={styles.reset} type="button">
				Réinitialiser
			</button>
		</div>
	);
};

export default Actions;
