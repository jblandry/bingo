const styles = {
	root: "fixed bottom-0 w-full p-2 mt-4 bg-gray-300 text-center",
	link: "text-blue-700 underline",
};

const Footer = function () {
	return (
		<footer className={styles.root}>
			&copy; 2022{" "}
			<a href="https://khaos.ca" target="_blank" rel="noreferrer" className={styles.link}>
				khaos.ca
			</a>
		</footer>
	);
};

export default Footer;
