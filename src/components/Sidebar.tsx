import { useAppContext } from "../contexts/AppContext";

const styles = {
	current:
		"block aspect-square w-48 m-auto bg-blue-700 px-4 rounded-full font-bold text-white text-5xl text-center leading-[4]",
	list: "grid grid-rows-latest grid-flow-col mt-8",
	item: "h-8 px-2 border-l border-gray-400 text-gray-400 text-center",
	item1: "font-bold !text-blue-700 text-2xl",
	item2: "font-bold !text-blue-600 text-xl",
	item3: "font-bold !text-blue-500 text-lg",
	item4: "font-bold !text-blue-400 text-lg",
	item5: "font-bold !text-blue-300",
	noborder: "border-l-0",
} as { [key: string]: string };

const withLetter = (value: number) => `${[..."BINGO"][Math.floor((value - 1) / 15)]}-${value}`;

const Sidebar = function () {
	const { getDrawn } = useAppContext();
	const list = [...getDrawn()];
	const current = list.shift();

	return current !== undefined ? (
		<>
			<strong className={styles.current}>{withLetter(current!)}</strong>
			<ul className={styles.list}>
				{list.map((value, index) => (
					<li
						key={value}
						className={`${styles.item} ${styles[`item${index < 5 ? index + 1 : ""}`]} ${
							index < 10 ? styles.noborder : ""
						}`}
					>
						{withLetter(value)}
					</li>
				))}
			</ul>
		</>
	) : (
		<> </>
	);
};

export default Sidebar;
