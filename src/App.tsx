import "./App.css";
import Actions from "./components/Actions";
import AppContextProvider from "./contexts/AppContext";
import Board from "./components/Board";
import Footer from "./components/Footer";
import Header from "./components/Header";
import Sidebar from "./components/Sidebar";

const styles = {
	root: "flex flex-row mb-8",
	main: "basis-2/3 m-8",
	side: "basis-1/3 m-8",
};

const App = function () {
	return (
		<AppContextProvider>
			<Header />
			<main className={styles.root}>
				<div className={styles.main}>
					<Actions />
					<Board />
				</div>
				<div className={styles.side}>
					<Sidebar />
				</div>
			</main>
			<Footer />
		</AppContextProvider>
	);
};

export default App;
