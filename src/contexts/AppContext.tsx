import React, { useContext, useMemo, useState } from "react";

import PropTypes from "prop-types";

type Marbles = number[];

type Context = {
	getMarbles(): Marbles;
	getRemaining(): Marbles;
	getDrawn(): Marbles;
	draw(): void;
	reset(): void;
};

const AppContext = React.createContext({} as Context);

const AppContextProvider = function ({ children }: { children: PropTypes.ReactNodeArray }) {
	const marbles = Array.from({ length: 75 }, (x, index) => index + 1);
	const [remaining, setRemaining] = useState([...marbles]);
	const [drawn, setDrawn] = useState([] as number[]);

	const values = useMemo(() => {
		const getMarbles = (): Marbles => {
			return [...marbles];
		};

		const getRemaining = (): Marbles => {
			return [...remaining];
		};

		const getDrawn = (): Marbles => {
			return [...drawn];
		};

		const draw = () => {
			const current = [...remaining];
			const position = Math.floor(Math.random() * current.length);
			const [marble] = current.splice(position, 1);
			setRemaining(current);
			setDrawn([marble, ...drawn]);
		};

		const reset = () => {
			setRemaining([...marbles]);
			setDrawn([]);
		};

		return {
			getMarbles,
			getRemaining,
			getDrawn,
			draw,
			reset,
		};
	}, [marbles, remaining, drawn]);

	return <AppContext.Provider value={values}>{children}</AppContext.Provider>;
};

AppContextProvider.propTypes = {
	children: PropTypes.node.isRequired,
};

export default AppContextProvider;
export const useAppContext = () => {
	return useContext(AppContext);
};
